fn main() {
    // &str
    let s = " hello ";
    // p becomes a substring of s
    // it's a slice, not a Vec
    // cannot be changed
    let p = s.trim();
    println!("p == '{}'", p);

    // memory has been copied onto the heap
    let mut ss = " hello ".to_string();

    // immutable borrow
    let pp = ss.trim();
    println!("pp == '{}'", pp);
    // after this pp is dropped I guess?

    // borrowed mutably
    ss.push_str(", world!");

    let f_str = "let us find an f";
    let f_result = string_find_f(f_str);
    println!("f_result = '{}'", f_result);

    println!("chosen str = '{};", choose_str(1));
}

// String is a str in a Box
fn string_find_f<'a>(s: &'a str) -> &'a str {
    // (i, x): (usize, char)
    // for (i, x) in s.chars().enumerate() {
    for (i, x) in s.char_indices() {
        if x == 'f' {
            return &s[i..];
        }
    }
    s
}

// 'static means it will never change as long as the program exists
// special lifetime
fn choose_str(n: i32) -> &'static str {
    match n {
        0 => "hello",
        1 => "goodbye",
        _ => "other",
    }
}
