#[derive(Debug, Clone)]
pub struct Person {
    name: String, // String does not implement Copy
    age: i32,
}

// Copy is basically Clone by default
#[derive(Debug, Clone, Copy)]
pub struct Point {
    x: i32,
    y: i32,
}

impl Person {
    pub fn new(name: String, age: i32) -> Self {
        Person { name, age }
    }

    // `&` means borrow?
    // four possible parameters:
    //   - `self`
    //   - `&self`
    //   - `&mut self`
    //   - `mut self`
    // if you just want to read it, never change it, and give it back, then use `&self`
    pub fn greet(&self) -> String {
        format!("Hi I am {}", self.name)
    }

    pub fn age_up(&mut self, n: i32) {
        self.age += n
    }

    // this function consumes `self` and never gives it back
    // after this function is called you cannot consume `self` anymore!
    pub fn drop_me(self) {}
}

// pointer to Person
// pointer to age
// the lifetime of the incoming pointer and that of the outgoing pointer must match
pub fn get_age(s: &Person) -> &i32 {
    &s.age
}

fn main() {
    let p = Person::new("Guangchu".to_string(), 29);
    let greeting = p.greet();
    println!("{}", greeting);
    let another_greeting = p.greet();
    println!("{}", another_greeting);

    let age = get_age(&p);
    println!("p's age is {}", age);

    let mut p2 = Person::new("Jovi".to_string(), 29);
    p2.age_up(6);
    println!("p2: {:?}", p2);
}
