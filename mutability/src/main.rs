#[derive(Debug, Clone)]
pub struct Person {
    name: String, // String does not implement Copy
    age: i32,
}

// Copy is basically Clone by default
#[derive(Debug, Clone, Copy)]
pub struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Self {
        Point { x, y }
    }
}

fn main() {
    let mut x = 29;
    let y = x; // copy
    x += 5;
    println!("y = {}, x = {}", x, y);

    let mut p = Person {
        name: "Guangchu".to_string(),
        age: 29,
    };
    let p2 = p.clone();
    p.name.push_str(" Shi");

    let pnt = Point::new(10, 26);
    let pnt2 = pnt; // because Copy implemented

    println!("p = {:?}; p2 = {:?}", p, p2);
    println!("pnt = {:?}; pnt2 = {:?}", pnt, pnt2);
}
