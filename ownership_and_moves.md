# Ownership and Moves

- `Box<T>` is a pointer to a value of type `T` stored on **heap**
  - calling `Box::new(v)` allocates some heap space, moves the value `v` into
    it, and returns a `Box` pointing to the heap space
- Very simple types like integers, floating-point numbers, and characters are
  excused from ownership rules
  - these are called `Copy` types
- the std lib provides reference-counted pointer types `Rc` and `Arc`, which
  allow values to have multiple owners, under some restrictions
- You can "borrow" a reference to a value
  - references are non-owning pointers, with limited lifetimes
- We call the action of creating a reference "**borrowing**"
- You can have **ONLY one** mutable reference to a particular piece of data at
  a time
- A reference's scope starts from where it's introduced and continues through
  the last time that reference is used
  - this capability of compiler is called **Non-Lexical Lifetimes (NLL)**
-
