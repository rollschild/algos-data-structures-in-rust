pub struct Stepper {
    curr: i32,
    step: i32,
    max: i32,
}

// used in the `for` loop
impl Iterator for Stepper {
    type Item = i32;
    // pointer to a changeable version of `self`
    fn next(&mut self) -> Option<i32> {
        if self.curr >= self.max {
            return None;
        }

        let res = self.curr;
        self.curr += self.step;
        return Option::Some(res);
    }
}

fn main() {
    let mut st = Stepper {
        curr: 1,
        step: 3,
        max: 25,
    };

    // forever
    loop {
        match st.next() {
            Some(v) => println!("loop {}", v),
            None => break,
        }
    }

    let mut st_2 = Stepper {
        curr: 2,
        step: 4,
        max: 32,
    };

    while let Some(n) = st_2.next() {
        println!("Number is {}", n);
    }

    let it = Stepper {
        curr: 5,
        step: 2,
        max: 18,
    };
    for i in it {
        println!("for loop {}", i);
    }

    // for i in 0..10 {
    //     println!("Number is {}", i);
    // }
}
