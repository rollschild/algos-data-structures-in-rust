#[derive(Debug)]
pub struct Person {
    name: String,
    age: i32,
    children: i32,
    fav_color: Color,
}

#[derive(Debug)]
pub enum Color {
    Red(String),
    Green,
    Blue,
}

impl Person {
    pub fn print(self) -> String {
        /*
         * without semicolon, it's auto returned
         */
        return format!(
            "My name is {}, and I'm {} years old. I have {} children",
            self.name, self.age, self.children,
        );
    }
}

fn main() {
    let person = Person {
        name: "Guangchu".to_string(),
        age: 18,
        children: 0,
        fav_color: Color::Green,
    };
    let color = Color::Red("what".to_string());
    match color {
        Color::Red(s) => println!("It's red!, {}", s),
        Color::Green => println!("It's green!"),
        Color::Blue => println!("It's blue!"),
    }

    println!("Hello, world from {:?}!", person);
    println!("Hello, world from {:?}!", person.print());
}
