#[derive(Debug)]
pub enum Res<T, E> {
    Thing(T),
    Error(E),
}

pub enum Option<T> {
    Some(T),
    None,
}

fn divide(a: i32, b: i32) -> Result<i32, String> {
    if b == 0 {
        return Result::Err("Cannot divide by zero".to_string());
    }
    Result::Ok(a / b)
}

fn main() {
    let a = divide(12, 4);
    let b = divide(6, 0);

    // `match` is exhaustive
    // match a {
    //     Res::Thing(v) => println!("value is {}", v),
    //
    //     // for any other case, don't do anything
    //     _ => {}
    // }

    if let Result::Ok(val) = a {
        println!("value is {}", val);
    }

    println!("a = {:?}; b = {:?}", a, b)
}
