#[derive(Debug)]
pub struct LinkedList<T> {
    data: T,
    next: Option<Box<LinkedList<T>>>,
}

impl<T: std::ops::AddAssign> LinkedList<T> {
    pub fn add_up(&mut self, n: T) {
        self.data += n;
    }
}

fn main() {
    let mut ll = LinkedList {
        data: 3,
        next: Some(Box::new(LinkedList {
            data: 2,
            next: None,
        })),
    };

    if let Some(ref mut v) = ll.next {
        v.add_up(42);
    }

    // three attributes:
    //   - length
    //   - capacity
    //   - pointer to the buffer
    // capacity: how much memory that's already been allocated to hold this
    // let mut v: Vec<String> = Vec::new();
    let mut v: Vec<String> = Vec::with_capacity(1024);
    v.push("hello".to_string());
    v.push("bye".to_string());
    println!("v.len = {}; v.capacity = {}", v.len(), v.capacity());

    for i in 0..1023 {
        v.push(i.to_string());
    }
    println!("v.len = {}; v.capacity = {}", v.len(), v.capacity());

    println!("LinkedList: {:?}", ll);
}
