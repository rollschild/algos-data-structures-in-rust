# Pointers

- `&x`
  - produces a reference to `x`
  - it **borrows a reference to `x`**
- `*r`
  - refers to the value that pointer `r` points to
- Rust references are _never_ null
- Two types:
  - `&T`
  - `&mut T`
    - a mutable, _exclusive_ reference
    - as long as the reference exists, you may **NOT** have any other
      references of _any_ kind to that value
- `Box`
  - the simplest way to allocate a value in heap
  -
