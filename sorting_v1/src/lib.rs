use crossbeam::thread;
use std::fmt::Debug;

mod rand;

/*
 * =======================
 * Bubble Sort
 * =======================
 */
/*
 * Time: O(n ^ 2) worst case
 * best case: O(n)
 */
pub fn bubble_sort<T: PartialOrd + Debug>(v: &mut [T]) {
    for p in 0..v.len() {
        let mut sorted = true;
        for i in 0..(v.len() - 1 - p) {
            if v[i] > v[i + 1] {
                v.swap(i, i + 1);
                sorted = false;
            }
        }
        println!("{:?}", v);
        if sorted {
            return;
        }
    }
}

/*
 * Time: O(nlogn)
 * space: O(n)
 */
pub fn merge_sort<T: PartialOrd + Debug>(mut v: Vec<T>) -> Vec<T> {
    // sort the left half
    // sort the right half
    // bring the sorted halves together

    if v.len() <= 1 {
        return v;
    }

    let mut res = Vec::with_capacity(v.len());

    // perform merge sort
    let b = v.split_off(v.len() / 2);
    // now v only has the first half
    let a = merge_sort(v);

    // shadowing
    let b = merge_sort(b);

    // bring them together
    let mut a_it = a.into_iter();
    let mut b_it = b.into_iter();
    let mut a_peek = a_it.next();
    let mut b_peek = b_it.next();

    loop {
        match a_peek {
            /*
             * If `a` has value, then check against `b`
             */
            /*
             * `ref`: bind by reference during pattern matching
             */
            Some(ref a_val) => match b_peek {
                Some(ref b_val) => {
                    if b_val < a_val {
                        res.push(b_peek.take().unwrap());
                        b_peek = b_it.next();
                    } else {
                        res.push(a_peek.take().unwrap());
                        a_peek = a_it.next();
                    }
                }
                None => {
                    /*
                     * Consume the rest of `a`
                     */
                    res.push(a_peek.take().unwrap());

                    // Extends a collection with the content of an iterator
                    res.extend(a_it);
                    return res;
                }
            },

            /*
             * Finished using `a`
             * just consume `b`
             */
            None => {
                if let Some(b_val) = b_peek {
                    res.push(b_val);
                }
                res.extend(b_it);
                return res;
            }
        }
    }
}

/*
 * =================================
 * Quick Sort
 * =================================
 */
pub fn partition<T: PartialOrd>(v: &mut [T]) -> usize {
    // let mut p = 0;
    let mut p = rand::rand(v.len());
    v.swap(p, 0);
    p = 0;

    for i in 1..v.len() {
        if v[i] < v[p] {
            v.swap(p + 1, i);
            v.swap(p, p + 1);
            p += 1;
        }
    }

    return p;
}

pub fn quick_sort<T: PartialOrd>(v: &mut [T]) {
    if v.len() <= 1 {
        return;
    }

    let p = partition(v);

    let (a, b) = v.split_at_mut(p);
    quick_sort(a);
    quick_sort(&mut b[1..]);
}

struct RawSend<T>(*mut [T]);
unsafe impl<T> Send for RawSend<T> {}

/*
 * Below won't compile on Rust 2021
 */
// pub fn threaded_quick_sort<T: 'static + PartialOrd + Debug + Send>(v: &mut [T]) {
//     if v.len() <= 1 {
//         return;
//     }
//
//     let p = partition(v);
//
/*
 * The compiler does NOT know for sure that this thread is going to finish _before_ the
 * function exits
 */
//     let (a, b) = v.split_at_mut(p);
//
//     // mutable raw pointer to a slice of T
//     let raw_ptr_a: *mut [T] = a as *mut [T];
//     let raw_ptr_s = RawSend(raw_ptr_a);
//     unsafe {
//         let handle = thread::spawn(move || {
//             threaded_quick_sort(&mut *raw_ptr_s.0);
//         });
//
//         threaded_quick_sort(&mut b[1..]);
//
//         // bring threads together
//         // shouldn't fail
//         handle.join().ok();
//     }
// }

/*
 * https://users.rust-lang.org/t/does-a-threaded-quick-sort-necessarily-require-unsafe-and-raw-pointers/49988
 */
pub fn threaded_quick_sort_safe<T: PartialOrd + Send>(v: &mut [T]) {
    if v.len() <= 1 {
        return;
    }

    let p = partition(v);

    let (a, b) = v.split_at_mut(p);

    thread::scope(|scope| {
        scope.spawn(|_| {
            threaded_quick_sort_safe(a);
        });
        threaded_quick_sort_safe(&mut b[1..]);
    })
    .unwrap(); // threads are implicitly joined here
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_bubble_sort() {
        let mut v = vec![100, -1, 44, 5, 6, 0, 12, -9];
        bubble_sort(&mut v);
        assert_eq!(v, vec![-9, -1, 0, 5, 6, 12, 44, 100]);
    }

    #[test]
    fn test_merge_sort() {
        let v = vec![100, -1, 44, 5, 6, 0, 12, -9];
        let v = merge_sort(v);
        assert_eq!(v, vec![-9, -1, 0, 5, 6, 12, 44, 100]);
    }

    #[test]
    fn test_partition() {
        let mut v = vec![12, 6, 8, -1, 3, 100, 50];
        let p = partition(&mut v);

        for x in 0..v.len() {
            assert!((v[x] < v[p]) == (x < p));
        }
    }

    #[test]
    fn test_quick_sort() {
        let mut v = vec![100, -1, 44, 5, 6, 0, 12, -9];
        quick_sort(&mut v);
        assert_eq!(v, vec![-9, -1, 0, 5, 6, 12, 44, 100]);
    }

    #[test]
    fn test_threaded_quick_sort() {
        let mut v = vec![100, -1, 44, 5, 6, 0, 12, -9];
        threaded_quick_sort_safe(&mut v);
        assert_eq!(v, vec![-9, -1, 0, 5, 6, 12, 44, 100]);
    }
}
